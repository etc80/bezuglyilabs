import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

/****
 A. Проверить, что количество в табе ‘featured’ главного блока сайта больше 300
 B. Кликнуть на главной странице Sing Up, проверить что на открывшейся странице присутствуют кнопки для входа из социальных сетей - Google, Facebook
 C. Кликнуть на главной странице любой вопрос из секции Top Questions и на открывшейся странице проверить, что вопрос был задан сегодня.
 D. Дополнительно: проверить, что на главной странице stackoverflow есть предложение о работе с зарплатой больше $ 100k :)
 (тест должен упасть, если такого предложения нет).
 */
public class TestStackoverflow {

    private static WebDriver driver;

    @BeforeClass
    public static void initDriver(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void closeDriver(){
        driver.quit();
    }

    @Before
    public void openMainPage(){
        String url = "http://stackoverflow.com/";
        driver.get(url);
        driver.manage().window().maximize();
    }

    @Test
    public void testA(){
        Assert.assertTrue("Counter is lower than 300!",300 < Integer.parseInt(driver.findElement(By.xpath("//*[@id='tabs']/a/span")).getText()));
    }

    @Test
    public void testB() throws Exception{
        driver.findElement(By.xpath(".//*[@id='tell-me-more']")).click();
        Assert.assertTrue("Facebook or Google login not displayed!",driver.findElement(By.xpath(".//*[@id='openid-buttons']//span[text()='Facebook']")).isDisplayed() && driver.findElement(By.xpath("//*[@id='openid-buttons']//span[text()='Google']")).isDisplayed());
    }

    @Test
    public void testC()throws  Exception {
        int random = (int)(Math.random()*95+1) ;
        driver.findElement(By.xpath("//*[@id='qlist-wrapper']/div/div["+random+"]//a[@class='question-hyperlink']")).click();
        Assert.assertTrue("Message is not created today",driver.findElement(By.xpath("//*[@id='qinfo']//b[contains(text(),'today')]")).isDisplayed());
    }

    @Test
    public void testD()throws Exception{
        Assert.assertTrue("No job with $100K",checkTextPresense("//*[@id='hireme']//div[contains(text(),'$')]"));
    }

    public boolean checkTextPresense(String path){
        List<WebElement> allTitles = driver.findElements(By.xpath(path));
        if (allTitles.size() >0) {
            for (WebElement w : allTitles) {
                String s = w.getText().toString();
                    String price = s.substring(s.indexOf("$"), s.length()-1);
                    if (Integer.parseInt(price.toUpperCase().replace('$', ' ').replace('K', ' ').trim()) > 100)
                        return true;
            }
        }

        return false;
    }

}
